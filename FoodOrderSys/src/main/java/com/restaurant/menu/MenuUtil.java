package com.restaurant.menu;

import com.restaurant.food.CuisineType;
import com.restaurant.food.Order;
import com.restaurant.service.DrinkOrderService;
import com.restaurant.service.LunchOrderService;

import java.util.Scanner;

/**
 * It's a utility class that provides methods for
 * command line UI.
 *
 * @author ersan yilmaz
 */
public class MenuUtil {

    public static int getSelectedOption(Scanner scanner) {
        int selectedOption;
        try {
            selectedOption = Integer.valueOf(scanner.next());
        } catch (NumberFormatException e) {
            throw new NumberFormatException("Not a proper selection in menu!");
        }
        return selectedOption;
    }

    public static Order orderLunch(LunchOrderService lunchOrderService, int selectedCuisine) {
        Order lunch = null;

        switch (selectedCuisine) {
            case 1:
                lunch = lunchOrderService.orderLunch(CuisineType.POLISH, "dessert");
                break;
            case 2:
                lunch = lunchOrderService.orderLunch(CuisineType.MEXICAN, "dessert");
                break;
            case 3:
                lunch = lunchOrderService.orderLunch(CuisineType.ITALIAN, "dessert");
                break;
        }
        return lunch;
    }

    public static Order orderDrink(DrinkOrderService drinkOrderService, int selectedOption) {
        Order drink = null;
        switch (selectedOption) {
            case 1:
                drink = drinkOrderService.orderDrink("Tea");
                break;
            case 2:
                drink = drinkOrderService.orderDrinkWithIce("Tea");
                break;
            case 3:
                drink = drinkOrderService.orderDrinkWithLemon("Tea");
                break;
            case 4:
                drink = drinkOrderService.orderDrinkWithIceAndLemon("Tea");
                break;
        }
        return drink;
    }

    public static void displayMainMenu() {
        System.out.println("");
        System.out.println("Main Menu");
        System.out.println("---------------------------");
        System.out.println("[1]. Cuisines");
        System.out.println("[2]. Drink");
        System.out.println("----------------------------");
        System.out.println("");
        System.out.println("Please select an option from 1-2");
        System.out.println("Any character other than shown above will exit!");
        System.out.println("");
    }

    public static void displayCuisines() {
        System.out.println("");
        System.out.println("Cuisines");
        System.out.println("---------------------------");
        System.out.println("[1]. Polish");
        System.out.println("[2]. Mexican");
        System.out.println("[3]. Italian");
        System.out.println("----------------------------");
        System.out.println("");
        System.out.println("Please select an option from 1-3");
        System.out.println("Any character other than shown above will exit!");
        System.out.println("");
    }

    public static void displayDrinkOptions() {
        System.out.println("");
        System.out.println("Drink Options");
        System.out.println("---------------------------");
        System.out.println("[1]. Plain");
        System.out.println("[2]. With ice");
        System.out.println("[3]. With lemon");
        System.out.println("[4]. With ice and lemon");
        System.out.println("----------------------------");
        System.out.println("");
        System.out.println("Please select an option from 1-4");
        System.out.println("Any character other than shown above will exit!");
        System.out.println("");
    }

    public static void printName(Order order) {
        System.out.println("Order: " + order.getName());
    }

    public static void printPrice(Order order) {
        System.out.println("Sum:" + order.getPrice());
    }
}
