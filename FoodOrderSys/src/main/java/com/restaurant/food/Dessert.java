package com.restaurant.food;

/**
 * It's a domain object for desserts
 *
 * @author ersan yilmaz
 */
class Dessert implements Order {

    private String name;
    private double price;

    Dessert(String name) {
        this.name = name;
    }

    @Override
    public double getPrice() {
        return this.price;
    }

    @Override
    public String getName() {
        return this.name;
    }
}
