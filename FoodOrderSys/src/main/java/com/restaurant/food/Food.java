package com.restaurant.food;

/**
 * The <code>Food</code> is a base class for all
 * cuisines. When a new cuisine is added to the menu,
 * this class must be extended.
 *
 * @author ersan yilmaz
 */
abstract class Food implements Order {
    String name;
    double price;
}
