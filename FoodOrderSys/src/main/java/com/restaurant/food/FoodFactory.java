package com.restaurant.food;

/**
 * It's a factory class which returns <code>Food</code>
 * object for a given cuisine type.
 *
 * @author ersan yilmaz
 */
class FoodFactory {

    private FoodFactory() {
    }

    /**
     * Returns a <code>Food</code> object.
     *
     * @param cuisineType cuisine from the <code>CuisineType</code>
     * @throws IllegalArgumentException if improper cuisine type passed
     */
    static Food cookFood(CuisineType cuisineType) {
        switch (cuisineType) {
            case POLISH:
                return new PolishFood();
            case MEXICAN:
                return new MexicanFood();
            case ITALIAN:
                return new ItalianFood();
            default:
                throw new IllegalArgumentException("Not proper cuisine chosen!");
        }
    }
}
