package com.restaurant.food;

/**
 * @author ersan yilmaz
 */
class MexicanFood extends Food {

    @Override
    public double getPrice() {
        return this.price;
    }

    @Override
    public String getName() {
        return "Mexican Food";
    }
}
