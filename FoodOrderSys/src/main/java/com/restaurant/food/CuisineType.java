package com.restaurant.food;

/**
 * It's class where types of cuisines are defined.
 * When a new cuisine is added to the system, the new
 * type must be added here also.
 *
 * @author ersan yilmaz
 */
public enum CuisineType {

    POLISH, MEXICAN, ITALIAN
}
