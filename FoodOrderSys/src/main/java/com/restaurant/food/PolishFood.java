package com.restaurant.food;

/**
 * @author ersan yilmaz
 */
class PolishFood extends Food {

    @Override
    public double getPrice() {
        return this.price;
    }

    @Override
    public String getName() {
        return "Polish Food";
    }
}
