package com.restaurant.food;

/**
 * The <code>Order</code> interface provides price
 * and name information for a given order.
 *
 * @author ersan yilmaz
 */
public interface Order {

    /**
     * Returns the price of given order.
     */
    double getPrice();

    /**
     * Returns the name of given order.
     */
    String getName();
}
