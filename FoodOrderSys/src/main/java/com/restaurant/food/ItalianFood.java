package com.restaurant.food;

/**
 * @author ersan yilmaz
 */
class ItalianFood extends Food {

    @Override
    public double getPrice() {
        return this.price;
    }

    @Override
    public String getName() {
        return "Italian food";
    }
}
