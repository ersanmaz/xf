package com.restaurant.food;

/**
 * @author ersan yilmaz
 */
public class Lunch implements Order {

    private Order mainDish;
    private Order dessert;

    /**
     * Initializes mainDish and dessert Order objects
     * for given cuisine type and dessert name
     *
     * @param cuisineType type of cuisine
     * @param dessertName name of dessert
     */
    public void prepareLunch(CuisineType cuisineType, String dessertName) {
        mainDish = FoodFactory.cookFood(cuisineType);
        dessert = new Dessert(dessertName);
    }

    /**
     * Returns sum of ordered main dish and dessert
     */
    @Override
    public double getPrice() {
        return mainDish.getPrice() + dessert.getPrice();
    }

    /**
     * Returns names of ordered main dish and dessert
     */
    @Override
    public String getName() {
        return mainDish.getName() + " and " + dessert.getName();
    }
}
