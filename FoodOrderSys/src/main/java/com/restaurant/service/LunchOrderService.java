package com.restaurant.service;

import com.restaurant.food.CuisineType;
import com.restaurant.food.Order;

/**
 * It's a service interface for lunch order
 *
 * @author ersan yilmaz
 */
public interface LunchOrderService {

    /**
     * Returns a lunch order for the given cuisine type
     * and dessert.
     *
     * @param cuisineType used for which food to be returned.
     * @return an order for lunch
     */
    Order orderLunch(CuisineType cuisineType, String dessertName);
}
