package com.restaurant.service;

import com.restaurant.food.Order;

/**
 * It's a service for drink order
 *
 * @author ersan yilmaz
 */
public interface DrinkOrderService {

    /**
     * Returns a drink order for a given drink name
     *
     * @param drink name of drink
     * @return an order for drink
     */
    Order orderDrink(String drink);

    /**
     * Returns a drink with ice for a given drink name
     *
     * @param drink name of drink
     * @return an order for drink
     */
    Order orderDrinkWithIce(String drink);

    /**
     * Returns a drink with lemon for a given drink name
     *
     * @param drink name of drink
     * @return an order for drink
     */
    Order orderDrinkWithLemon(String drink);

    /**
     * Returns a drink with ice and lemon for a given drink name
     *
     * @param drink name of drink
     * @return an order for drink
     */
    Order orderDrinkWithIceAndLemon(String drink);
}
