package com.restaurant.service;

import com.restaurant.drink.Drink;
import com.restaurant.food.Order;

/**
 * Created by ersan on 16.08.2016
 */
public class DrinkOrderServiceBean implements DrinkOrderService {

    @Override
    public Order orderDrink(String drink) {
        Drink.Builder builder = new Drink.Builder(0.0, drink);
        return builder.build();
    }

    @Override
    public Order orderDrinkWithIce(String drink) {
        Drink.Builder builder = new Drink.Builder(0.0, drink);
        builder.withIce(true);
        return builder.build();
    }

    @Override
    public Order orderDrinkWithLemon(String drink) {
        Drink.Builder builder = new Drink.Builder(0.0, drink);
        builder.withLemon(true);
        return builder.build();
    }

    @Override
    public Order orderDrinkWithIceAndLemon(String drink) {
        Drink.Builder builder = new Drink.Builder(0.0, drink);
        builder.withIce(true);
        builder.withLemon(true);
        return builder.build();
    }
}
