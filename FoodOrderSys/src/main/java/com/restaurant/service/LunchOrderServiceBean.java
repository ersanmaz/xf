package com.restaurant.service;

import com.restaurant.food.CuisineType;
import com.restaurant.food.Lunch;
import com.restaurant.food.Order;

/**
 * @author ersan yilmaz
 */
public class LunchOrderServiceBean implements LunchOrderService {

    @Override
    public Order orderLunch(CuisineType cuisineType, String dessertName) {
        Lunch lunch = new Lunch();
        lunch.prepareLunch(cuisineType, dessertName);
        return lunch;
    }
}