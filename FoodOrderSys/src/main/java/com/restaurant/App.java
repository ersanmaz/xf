package com.restaurant;

import com.restaurant.food.Order;
import com.restaurant.service.DrinkOrderService;
import com.restaurant.service.DrinkOrderServiceBean;
import com.restaurant.service.LunchOrderService;
import com.restaurant.service.LunchOrderServiceBean;

import java.util.Scanner;

import static com.restaurant.menu.MenuUtil.*;

/**
 * @author ersan yilmaz
 */
public class App {

    public static void main(String[] args) {

        LunchOrderService lunchOrderService = new LunchOrderServiceBean();
        DrinkOrderService drinkOrderService = new DrinkOrderServiceBean();

        Scanner scanner = new Scanner(System.in);
        displayMainMenu();
        int selectedOption;
        selectedOption = getSelectedOption(scanner);

        Order order;
        if (selectedOption == 1) {
            displayCuisines();
            int selectedCuisine = getSelectedOption(scanner);
            order = orderLunch(lunchOrderService, selectedCuisine);
        } else {
            displayDrinkOptions();
            int selectedDrink = getSelectedOption(scanner);
            order = orderDrink(drinkOrderService, selectedDrink);
        }

        printName(order);
        printPrice(order);
    }
}