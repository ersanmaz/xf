package com.restaurant.drink;

import com.restaurant.food.Order;

/**
 * It's drink domain class used for drink order.
 *
 * @author ersan yilmaz
 */
public class Drink implements Order {

    private double price;
    private String name;
    private boolean ice;
    private boolean lemon;

    private Drink(Builder builder) {
        this.price = builder.price;
        this.name = builder.name;
        this.ice = builder.ice;
        this.lemon = builder.lemon;
    }

    @Override
    public double getPrice() {
        return this.price;
    }

    @Override
    public String getName() {
        StringBuilder drink = new StringBuilder();
        drink.append(this.name);

        if (this.ice) {
            drink.append(" With Ice");
        }
        if (this.lemon) {
            drink.append(" With Lemon");
        }
        return drink.toString();
    }

    public static class Builder {

        private double price;
        private String name;
        private boolean ice;
        private boolean lemon;

        public Builder(double price, String name) {
            this.price = price;
            this.name = name;
        }

        public Builder withIce(boolean ice) {
            this.ice = ice;
            return this;
        }

        public Builder withLemon(boolean lemon) {
            this.lemon = lemon;
            return this;
        }

        public Drink build() {
            return new Drink(this);
        }
    }
}
