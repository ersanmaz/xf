package com.restaurant.service;

import com.restaurant.food.CuisineType;
import com.restaurant.food.Order;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author ersan yilmaz
 */
public class LunchOrderServiceTest {

    private LunchOrderService lunchOrderService;

    @Before
    public void setUp() {
        lunchOrderService = new LunchOrderServiceBean();
    }

    @Test
    public void orderLunch() {
        Order lunch = lunchOrderService.orderLunch(CuisineType.ITALIAN, "Pudding");
        String lunchName = lunch.getName();
        String expectedLunchName = "Italian food and Pudding";
        assertEquals(lunchName, expectedLunchName);
    }

}