package com.restaurant.service;

import com.restaurant.food.Order;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @author ersan yilmaz
 */
public class DrinkOrderServiceTest {

    private DrinkOrderService drinkOrderService;

    @Before
    public void setUp() {
        drinkOrderService = new DrinkOrderServiceBean();
    }

    @Test
    public void orderDrink() {
        Order tea = drinkOrderService.orderDrink("Tea");
        String expectedDrink = "Tea";
        assertEquals(expectedDrink, tea.getName());

        String unexpectedDrink = "Coffee";
        assertNotEquals(unexpectedDrink, tea.getName());
    }

    @Test
    public void orderDrinkWithIce() {
        Order tea = drinkOrderService.orderDrinkWithIce("Tea");
        String expectedDrink = "Tea With Ice";
        assertEquals(expectedDrink, tea.getName());

        String unexpectedDrink = "Coffee With Ice";
        assertNotEquals(unexpectedDrink, tea.getName());
    }

    @Test
    public void orderDrinkWithLemon() {
        Order tea = drinkOrderService.orderDrinkWithLemon("Tea");
        String expectedDrink = "Tea With Lemon";
        assertEquals(expectedDrink, tea.getName());

        String unexpectedDrink = "Coffee With Lemon";
        assertNotEquals(unexpectedDrink, tea.getName());
    }

    @Test
    public void orderDrinkWithIceAndLemon() {
        Order tea = drinkOrderService.orderDrinkWithIceAndLemon("Tea");
        String expectedDrink = "Tea With Ice With Lemon";
        assertEquals(expectedDrink, tea.getName());

        String unexpectedDrink = "Coffee With Ice With Lemon";
        assertNotEquals(unexpectedDrink, tea.getName());
    }
}