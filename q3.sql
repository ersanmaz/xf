select * 
from XF.Country co
where co.CountryID in 
	(select c.CountryID 
	 from XF.City c 
	 group by c.CountryID 
	 having sum(c.Population) > 400);

select co.Name 
from XF.Country co
where co.CountryID not in 
	(select c.CountryID
	 from XF.City c 
	 where c.CityID in 
		(select b.CityID 
		 from XF.Building b
		 where b.CityID = c.CityID));