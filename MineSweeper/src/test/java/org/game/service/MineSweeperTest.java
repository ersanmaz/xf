package org.game.service;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

/**
 * @author ersan yilmaz
 */
public class MineSweeperTest {

    private MineSweeper mineSweeper;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup() {
        mineSweeper = new MineSweeperBean();
    }

    @Test
    public void testInvalidInput() {
        String mineField = "...a**\n*.*...\n.....*\n..**..";
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage("Given input doesn't contain valid characters or has incompatible structure");
        mineSweeper.setMineField(mineField);
    }

    @Test
    public void testBoardInitialization() {
        thrown.expect(IllegalStateException.class);
        thrown.expectMessage("Game is not initialized.");
        mineSweeper.getHintField();
    }

    @Test
    public void getHintField() throws Exception {
        String mineField = "....**\n*.*...\n.....*\n..**..";
        mineSweeper.setMineField(mineField);

        String expectedHintField = "1212**\\n*2*233\\n13332*\\n01**21";
        String generatedHintField = mineSweeper.getHintField();
        assertEquals(expectedHintField, generatedHintField);
    }
}