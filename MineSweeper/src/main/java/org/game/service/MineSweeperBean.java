package org.game.service;

import org.game.model.Board;

/**
 * @author ersan yilmaz
 */
public class MineSweeperBean implements MineSweeper {

    private Board board;

    @Override
    public void setMineField(String mineField) throws IllegalArgumentException {
        board = Board.createBoard(mineField);
    }

    @Override
    public String getHintField() throws IllegalStateException {
        if (board == null) {
            throw new IllegalStateException("Game is not initialized.");
        }
        return board.getHintMineField();
    }
}
