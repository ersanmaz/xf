package org.game.model;

/**
 * @author ersan yilmaz
 */
public final class Board {

    private static final String REGEX = ".(\\*|\\.|\\n)*";
    private static final String NEW_LINE = "\n";

    private int height;
    private int width;
    private char[][] mineField;

    private Board() {
    }

    public static Board createBoard(String mineFieldInput) {
        Board board = new Board();
        board.setMineField(mineFieldInput);
        return board;
    }

    /**
     * Produces hint field. It contains '*' and
     * numbers which is showing number of mines
     * in adjacent cells.
     */
    public final String getHintMineField() {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (mineField[i][j] == '*') {
                    sb.append("*");
                } else {
                    sb.append(getNumberOfMinesInAdjacentCells(i, j));
                }
            }
            sb.append("\\n");
        }
        sb.setLength(sb.length() - 2); // removes \n at the end
        return sb.toString();
    }

    /**
     * Sets the mine field according to given input.
     *
     * @param mineFieldInput given input value
     * @throws IllegalArgumentException if input is not valid
     */
    private void setMineField(String mineFieldInput) {
        if (!isValidInput(mineFieldInput)) {
            throw new IllegalArgumentException("Given input doesn't contain valid characters or has incompatible structure");
        }

        String[] lines = mineFieldInput.split(NEW_LINE);
        height = lines.length;
        width = lines[0].length();
        mineField = new char[height][width];
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                mineField[i][j] = lines[i].charAt(j);
            }
        }
    }

    /**
     * Returns number of mines found in adjacent cells.
     *
     * @param column position in x-axis.
     * @param row position in y-axis.
     */
    private int getNumberOfMinesInAdjacentCells(int column, int row) {
        int count = 0;
        count += mineInPosition(column, row - 1);
        count += mineInPosition(column, row + 1);
        count += mineInPosition(column - 1, row + 1);
        count += mineInPosition(column - 1, row);
        count += mineInPosition(column - 1, row - 1);
        count += mineInPosition(column + 1, row + 1);
        count += mineInPosition(column + 1, row);
        count += mineInPosition(column + 1, row - 1);
        return count;
    }

    /**
     * Returns 1 if a mine is found in given position
     * and returns 0 if it's not.
     *
     * @param column position in x-axis.
     * @param row position in y-axis.
     */
    private int mineInPosition(int column, int row) {
        if (isExceededLimits(column, height) && isExceededLimits(row, width) && mineField[column][row] == '*') {
            return 1;
        }
        return 0;
    }

    /**
     * Checks whether the given value is
     * between 0 and limit.
     *
     * @param checkedValue to be checked if it's in limit
     * @param limit max value for given input
     */
    private boolean isExceededLimits(int checkedValue, int limit) {
        return 0 <= checkedValue && checkedValue < limit;
    }

    /**
     * Checks validity of given string input.
     * The input should contain only '*', '.' and '\n'
     * characters.
     *
     * @param mineFieldInput to be checked validity.
     */
    private boolean isValidInput(String mineFieldInput) {

        if (mineFieldInput == null || "".equals(mineFieldInput)) {
            return false;
        }

        if (mineFieldInput.matches(REGEX)) {
            String[] lines = mineFieldInput.split(NEW_LINE);
            return lines.length == 1 || areRowSizesEqual(lines);
        }

        return false;
    }

    /**
     * Checks whether the number of characters
     * in each line are equal.
     *
     * @param lines where '*' and '.' are located.
     */
    private boolean areRowSizesEqual(String[] lines) {
        int sizeOfFirstLine = lines[0].length();
        for (int i = 1; i < lines.length; i++) {
            if (sizeOfFirstLine != lines[i].length()) {
                return false;
            }
        }
        return true;
    }
}
